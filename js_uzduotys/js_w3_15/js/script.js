function skirtumas(sk) {
    if (sk < 13)
    {
        return 13 - sk;
    }
    else
    {
        return (sk - 13) * 2;
    }
}

console.log(skirtumas(55)); // 84
console.log(skirtumas(2)); // 11
console.log(skirtumas(13)); // 0