function absolute(sk) {
    if(sk >= 19)
    {
        return (sk -19) * 3;
    }
    else
    {
        return 19 - sk;
    }
}

console.log(absolute(22)); // 39
console.log(absolute(11)); // 17
console.log(absolute(19)); // 2085

