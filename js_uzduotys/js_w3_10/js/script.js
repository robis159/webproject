var inp_1 = document.getElementById('1st');
var inp_2 = document.getElementById('2nd');
var butt_mult = document.getElementById('mult');
var butt_divi = document.getElementById('divi');
var res = document.getElementById('result');


butt_mult.onclick = function () {
    res.textContent = inp_1.value * inp_2.value;
}

butt_divi.onclick = function () {
    res.textContent = inp_1.value / inp_2.value;
}