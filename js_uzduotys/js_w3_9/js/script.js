var today = new Date();
var month = today.getMonth();
var untilNow = 0;
var thisYer = 0;

function daysInMonth(month,year) {
    return new Date(year, month, 0).getDate();
}

for(i = 0; i < month; i++)
{
    untilNow += daysInMonth(i + 1, today.getFullYear())
}

for(i = 0; i < 12; i++)
{
    thisYer += daysInMonth(i + 1, today.getFullYear())
}

var leftDays = thisYer - untilNow;

document.getElementById('par').textContent = 'liko ' + leftDays + ' days until new year.';
