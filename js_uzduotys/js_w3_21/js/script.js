function add_py(str) {
    if (str.substring(0, 2) === 'py')
    {
        return str;
    }
    else
    {
        return 'py' + str;
    }
}

console.log(add_py('labas'));
console.log(add_py('pyragas'));
console.log(add_py('vol'));
console.log(add_py('thon'));
