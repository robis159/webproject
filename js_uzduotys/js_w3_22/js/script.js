
function character_remove(word, char_position)
{
    var texend = word.slice(char_position);
    var textfront = word.slice(0, char_position - 1);

    return textfront + texend;
    
}

console.log(character_remove('laba diena', 1));
console.log(character_remove('laba diena', 2));
console.log(character_remove('laba diena', 3));
console.log(character_remove('laba diena', 4));
console.log(character_remove('laba diena', 5));
console.log(character_remove('laba diena', 6));


