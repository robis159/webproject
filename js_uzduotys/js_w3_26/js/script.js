function take_last_3_char(str) {
    if(str.length <= 3)
    {
        return str;
    }
    var char = str.substr(-3);
    return char + str + char;
}

console.log(take_last_3_char('teta'));
console.log(take_last_3_char('pasikiskekopusteliaudamas'));
console.log(take_last_3_char('labas vakaras'));
