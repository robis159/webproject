function suma(sk1, sk2) {
    if(sk1 == sk2)
    {
        return sk1 * 3;
    }
    else
    {
        return sk1 + sk2;
    }
}

console.log(suma(13,13)); // 39
console.log(suma(1,16)); // 17
console.log(suma(99,1986)); // 2085
console.log(suma(1892,1892)); // 5676
console.log(suma(566,1258)); // 1824
